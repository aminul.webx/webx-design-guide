import React, { useEffect } from "react";
import "../sass/custom/sidenav.scss";

const SideNav = (props: any) => {
  //   const handleClick = () => {};

  const { children } = props;
  // console.log(props);

  useEffect(() => {
    var sideNav: any = document.querySelector(".wx__side__nav");
    if (props.className) {
      sideNav.classList.add(props.className);
    }
    var singleNav = sideNav.querySelectorAll(".wx__single__side__nav");

    for (var i = 0; i < singleNav.length; i++) {
      singleNav[i].addEventListener("click", function (e: any) {
        var current = document.getElementsByClassName("active");
        if (current[0]) {
          current[0].classList.remove("active");
        }
        e.target.className += " active";
      });
    }
  }, []);

  return (
    <>
      <div className="wx__side__nav">{children}</div>
    </>
  );
};

export default SideNav;
