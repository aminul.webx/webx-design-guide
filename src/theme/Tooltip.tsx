import React from "react";
import "../sass/custom/wxtooltip.scss";
import WXTooltip from "./WXTooltip";

const Tooltip = () => {
  return (
    <>
      <div className="py-3">
        <span className="">Tooltip Example</span>
        <div style={{ width: "200px" }} className="d-flex flex-column m-auto">
          <WXTooltip message="Tooltip on Left" place="left">
            <span>Hover to see Tooltip on Left</span>
          </WXTooltip>
        </div>
      </div>
      <div></div>

      <WXTooltip message="Diffrent Message" place="right">
        <span>Hover to see Tooltip on Right</span>
      </WXTooltip>
    </>
  );
};

export default Tooltip;
