import React from "react";
import "../sass/custom/tags.scss";

const Tags = () => {
  return (
    <>
      <div className="d-flex justify-content-center my-5">
        <span>***Fixed***</span>
        <div>
          <button className="wx__btn_tags wx__fixed_tags mx-3">Default</button>
          <button className="wx__btn_tags_secondary wx__fixed_tags mx-3">
            Secondary
          </button>
          <button className="wx__btn_tags_warning wx__fixed_tags mx-3">
            Warning
          </button>
          <button className="wx__btn_tags_primary wx__fixed_tags mx-3">
            Primary
          </button>
          <button className="wx__btn_tags_success wx__fixed_tags mx-3">
            Success
          </button>
          <button className="wx__btn_tags_info wx__fixed_tags mx-3">
            Info
          </button>
          <button className="wx__btn_tags_danger wx__fixed_tags mx-3">
            Danger
          </button>
        </div>
      </div>
      <div className="d-flex justify-content-center my-5">
        <span>***Responsive***</span>
        <div>
          <button className="wx__btn_tags mx-3">Default</button>
          <button className="wx__btn_tags_secondary mx-3">Secondary</button>
          <button className="wx__btn_tags_warning  mx-3">Warning</button>
          <button className="wx__btn_tags_primary  mx-3">Primary</button>
          <button className="wx__btn_tags_success  mx-3">Success</button>
          <button className="wx__btn_tags_info  mx-3">Info</button>
          <button className="wx__btn_tags_danger  mx-3">Danger</button>
        </div>
      </div>
    </>
  );
};

export default Tags;
