import React from "react";
import { wxToastSlice } from "./WXToast";

const code = (
  <div className="col">
    <p>Info Message Another Example</p>
    <div className="wx__info__danger">
      <div className="wx__info__body">
        <span className="material-icons-round wx__info__message__front__icon">
          report_gmailerrorred
        </span>
        <span className="wx__info__message-part">
          A site with this domain name already exists.
        </span>
      </div>
    </div>
  </div>
);

const Toast = () => {
  const wXDClick = () => {
    wxToastSlice("hello World", {});
  };
  const wXSClick = () => {
    wxToastSlice("hello World", {
      // position: "top-center",
      type: "success",
    });
  };
  const wXEClick = () => {
    wxToastSlice("hello World", {
      // position: "top-center",
      type: "danger",
    });
  };
  const wXWClick = () => {
    wxToastSlice("hello World", {
      // position: "top-center",
      type: "warning",
    });
  };

  return (
    <div className="p-4">
      <div className="row">
        <p>Tost Alert Example</p>
        <div className="col">
          <div>
            <button
              onClick={wXDClick}
              className="wx__btn wx__btn_secondary mb-2"
            >
              Toast Default
            </button>
            {/* toast default */}
            <div className="wx__single__toast">
              <div className="wx__single__toast__body">
                <span className="wx__single__toast__message-part">
                  A simple primary alert__check it out!
                </span>
              </div>
              <span
                onClick={(e: any) => e.target.parentNode.remove()}
                className="material-icons-round wx__single__toast__close__btn"
              >
                close
              </span>
            </div>
          </div>
          <div>
            <button
              onClick={wXSClick}
              style={{ background: "green" }}
              className="wx__btn_sm  wx__btn_primary mb-2"
            >
              Toast Success
            </button>
            <div className="wx__single__toast-success">
              <div className="wx__single__toast__body">
                <span className="material-icons-round wx__single__toast__front__icon">
                  check_circle
                </span>
                <span className="wx__single__toast__message-part">
                  A simple primary alert__check it out!
                </span>
              </div>
              <span
                onClick={(e: any) => e.target.parentNode.remove()}
                className="material-icons-round wx__single__toast__close__btn"
              >
                close
              </span>
            </div>
          </div>
        </div>
        <div className="col">
          <div>
            <button
              onClick={wXEClick}
              style={{ background: "red" }}
              className="wx__btn_sm  wx__btn_danger mb-2"
            >
              Toast Danger
            </button>
            <div className="wx__single__toast-danger">
              <div className="wx__single__toast__body">
                <span className="material-icons-round wx__single__toast__front__icon">
                  cancel1
                </span>
                <span className="wx__single__toast__message-part">
                  A simple primary alert__check it out!
                </span>
              </div>
              <span
                onClick={(e: any) => e.target.parentNode.remove()}
                className="material-icons-round wx__single__toast__close__btn"
              >
                close
              </span>
            </div>
          </div>
          <div>
            <button
              onClick={wXWClick}
              style={{ background: "yellow", color: "black" }}
              className="btn btn-warning mb-2"
            >
              Toast Warning
            </button>
            <div className="wx__single__toast-warning">
              <div className="wx__single__toast__body">
                <span className="material-icons-round wx__single__toast__front__icon">
                  warning
                </span>
                <span className="wx__single__toast__message-part">
                  A simple primary alert__check it out!
                </span>
              </div>
              <span
                onClick={(e: any) => e.target.parentNode.remove()}
                className="material-icons-round wx__single__toast__close__btn"
              >
                close
              </span>
            </div>
          </div>
        </div>
        <iframe
          width="100%"
          height="300"
          title="s"
          src="//jsfiddle.net/TituX/1osa3nek/17/embedded/js/"
          allowFullScreen
        ></iframe>
      </div>
      <div className="row">
        <span>Copy the code below for Info Message</span>
        <div className="col">
          <p>Info Message Example</p>
          <div className="wx__info">
            <div className="wx__info__body">
              <span className="material-icons-round wx__info__message__front__icon">
                error
              </span>
              <span className="wx__info__message-part">
                User will get password through email
              </span>
            </div>
          </div>
          <iframe
            width="100%"
            title="s"
            height="300"
            src="//jsfiddle.net/TituX/t1fd95z7/3/embedded/html/"
            allowFullScreen
          ></iframe>
        </div>
        <div className="col">
          <p>Info Message Another Example</p>
          <div className="wx__info__danger">
            <div className="wx__info__body">
              <span className="material-icons-round wx__info__message__front__icon">
                report_gmailerrorred
              </span>
              <span className="wx__info__message-part">
                A site with this domain name already exists.
              </span>
            </div>
          </div>
          <iframe
            width="100%"
            title="s"
            height="300"
            src="//jsfiddle.net/TituX/t1fd95z7/5/embedded/html/"
            allowFullScreen
          ></iframe>
        </div>
      </div>
    </div>
  );
};

export default Toast;
