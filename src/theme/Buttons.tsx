import React from "react";

const Buttons = ({ clickToCopy }: any) => {
  // this.state.textToCopy
  return (
    <div
      className="my-5 mx-auto rounded p-4"
      style={{ border: "2px dashed blue" }}
    >
      {/* <marquee
        direction="down"
        width="250"
        height="200"
        behavior="alternate"
        style="border:solid"
      >
        <marquee behavior="alternate">This text will bounce</marquee>
      </marquee> */}
      <p>Click to Copy the Class Name</p>
      <div className="row">
        <div className="row row-cols-2">
          <div className="col">
            <div className="row">
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_sm wx__btn_primary"
                >
                  Default button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn wx__btn_primary"
                >
                  Default button
                </button>
                <button onClick={(e) => clickToCopy(e)} className="wx__btn">
                  This is button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_lg wx__btn_primary"
                >
                  Default button
                </button>
              </div>
            </div>
            <div className="row  mt-2">
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_sm wx__btn_secondary"
                >
                  Secondary Button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn wx__btn_secondary"
                >
                  Secondary Button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_lg wx__btn_secondary"
                >
                  Secondary Button
                </button>
              </div>
            </div>
            <div className="row  mt-2">
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_sm wx__btn_danger"
                >
                  Danger Button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn wx__btn_danger"
                >
                  Danger Button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_lg wx__btn_danger"
                >
                  Danger Button
                </button>
              </div>
            </div>
          </div>
          <div className="col">
            <div className="row ">
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_sm wx__btn_primary_outline"
                >
                  Default button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn wx__btn_primary_outline"
                >
                  Default button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_lg wx__btn_primary_outline"
                >
                  Default button
                </button>
              </div>
            </div>
            <div className="row mt-2">
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_sm wx__btn_secondary_outline"
                >
                  Secondary Button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn wx__btn_secondary_outline"
                >
                  Secondary Button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_lg wx__btn_secondary_outline"
                >
                  Secondary Button
                </button>
              </div>
            </div>
            <div className="row  mt-2">
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_sm wx__btn_danger_outline"
                >
                  Danger Button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn wx__btn_danger_outline"
                >
                  Danger Button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_lg wx__btn_danger_outline"
                >
                  Danger Button
                </button>
              </div>
            </div>
          </div>
          <div className="col mt-3">
            <div className="row ">
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_sm wx__btn_primary_outline_none"
                  disabled
                >
                  Default button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn wx__btn_primary_outline_none"
                >
                  Default button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_lg wx__btn_primary_outline_none"
                >
                  Default button
                </button>
              </div>
            </div>
            <div className="row mt-2">
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_sm wx__btn_secondary_outline_none"
                >
                  Secondary Button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn wx__btn_secondary_outline_none"
                >
                  Secondary Button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_lg wx__btn_secondary_outline_none"
                >
                  Secondary Button
                </button>
              </div>
            </div>
            <div className="row  mt-2">
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_sm wx__btn_danger_outline_none"
                >
                  Danger Button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn wx__btn_danger_outline_none"
                >
                  Danger Button
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_lg wx__btn_danger_outline_none"
                >
                  Danger Button
                </button>
              </div>
            </div>
          </div>
          {/* buttons with icons */}
          <div className="col mt-3">
            <div className="row ">
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_sm wx__btn_primary d-flex align-items-center"
                >
                  <span className="material-icons-round me-3">
                    calendar_today
                  </span>
                  small button
                  <span className="material-icons-round ms-1">
                    arrow_drop_down
                  </span>
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_sm wx__btn_primary_outline d-flex align-items-center"
                >
                  <span className="material-icons-round me-3">
                    calendar_today
                  </span>
                  small button
                  <span className="material-icons-round ms-1">
                    arrow_drop_down
                  </span>
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_sm wx__btn_primary_outline_none d-flex align-items-center "
                >
                  <span className="material-icons-round me-3">
                    calendar_today
                  </span>
                  small button
                  <span className="material-icons-round ms-1">
                    arrow_drop_down
                  </span>
                </button>
              </div>
            </div>
            <div className="row mt-2">
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn wx__btn_secondary d-flex align-items-center"
                >
                  <span className="material-icons-round me-2">
                    calendar_today
                  </span>
                  Secondary button
                  <span className="material-icons-round ms-1">
                    arrow_drop_down
                  </span>
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn wx__btn_secondary_outline d-flex align-items-center"
                >
                  <span className="material-icons-round me-2">
                    calendar_today
                  </span>
                  Secondary button
                  <span className="material-icons-round ms-1">
                    arrow_drop_down
                  </span>
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn wx__btn_secondary_outline_none d-flex align-items-center"
                >
                  <span className="material-icons-round me-2">
                    calendar_today
                  </span>
                  Secondary button
                  <span className="material-icons-round ms-1">
                    arrow_drop_down
                  </span>
                </button>
              </div>
            </div>
            <div className="row mt-2">
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_lg wx__btn_danger d-flex align-items-center"
                >
                  <span className="material-icons-round me-2">
                    calendar_today
                  </span>
                  Large button
                  <span className="material-icons-round ms-1">
                    arrow_drop_down
                  </span>
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_lg wx__btn_danger_outline d-flex align-items-center"
                >
                  <span className="material-icons-round me-2">
                    calendar_today
                  </span>
                  Large button
                  <span className="material-icons-round ms-1">
                    arrow_drop_down
                  </span>
                </button>
              </div>
              <div className="col">
                <button
                  onClick={(e) => clickToCopy(e)}
                  className="wx__btn_lg wx__btn_danger_outline_none d-flex align-items-center"
                >
                  <span className="material-icons-round me-2">
                    calendar_today
                  </span>
                  Large button
                  <span className="material-icons-round ms-1">
                    arrow_drop_down
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <p>Button Group Primary</p>
      <div
        className="wx__btn_group_primary"
        role="group"
        aria-label="Basic outlined example"
      >
        <button type="button" className="wx__btn wx__btn_primary_outline">
          Left
        </button>
        <button type="button" className="wx__btn wx__btn_primary_outline">
          Mid-Left
        </button>
        <button type="button" className="wx__btn wx__btn_primary_outline">
          Mid-Right
        </button>
        <button type="button" className="wx__btn wx__btn_primary_outline">
          Right
        </button>
      </div>
      <p className="mt-3">Button Group Secondary</p>
      <div
        className="wx__btn_group_secondary"
        role="group"
        aria-label="Basic outlined example"
      >
        <button type="button" className="wx__btn wx__btn_secondary_outline">
          Left
        </button>
        <button type="button" className="wx__btn wx__btn_secondary_outline">
          Mid-Left
        </button>
        <button type="button" className="wx__btn wx__btn_secondary_outline">
          Mid-Right
        </button>
        <button type="button" className="wx__btn wx__btn_secondary_outline">
          Right
        </button>
      </div>
      <p className="mt-3">Button Group Danger</p>
      <div
        className="wx__btn_group_danger"
        role="group"
        aria-label="Basic outlined example"
      >
        <button type="button" className="wx__btn wx__btn_danger_outline">
          Left
        </button>
        <button type="button" className="wx__btn wx__btn_danger_outline">
          Mid-Left
        </button>
        <button type="button" className="wx__btn wx__btn_danger_outline">
          Mid-Right
        </button>
        <button type="button" className="wx__btn wx__btn_danger_outline">
          Right
        </button>
      </div>
    </div>
  );
};

export default Buttons;
