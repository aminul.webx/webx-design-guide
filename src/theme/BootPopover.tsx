import React from "react";
import { Button, OverlayTrigger, Popover } from "react-bootstrap";

type Placement = "top" | "right" | "bottom" | "left";

interface IBootPopover {
  placement?: Placement;
}

const BootPopover = () => {
  const top = "top";
  return (
    <div>
      {["top", "right", "bottom", "left"].map((placement: string) => (
        <OverlayTrigger
          trigger="click"
          key={placement}
          // placement={placement}
          overlay={
            <Popover id={`popover-positioned-${placement}`}>
              <Popover.Header as="h3">{`Popover ${placement}`}</Popover.Header>
              <Popover.Body>
                <strong>Holy guacamole!</strong> Check this info.
              </Popover.Body>
            </Popover>
          }
        >
          <Button variant="secondary">Popover on {placement}</Button>
        </OverlayTrigger>
      ))}
    </div>
  );
};

export default BootPopover;
