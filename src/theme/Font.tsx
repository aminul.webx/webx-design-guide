import axios from "axios";
import React from "react";
import { baseUrl } from "../BaseURL";
const Font = ({ clickToCopy }: any) => {
  const handleLogin = () => {
    axios
      .post(`${baseUrl}/merchant/get-user-store-list`, {
        user_name: "sarker",
        password: "sarker",
      })
      .then((res) => console.log(res.data))
      .catch((err) => console.log(err));
  };

  const handleRegister = () => {
    axios
      .post(`${baseUrl}/merchant/sign-up`, {
        user_name: "webx_second",
        shop_name: "webx_eCommerce_second",
        shop_path: "/webx_eCommerce_second",
        password: "webx1234",
      })
      .then((res) => console.log(res.data))
      .catch((err) => console.log(err));
  };

  return (
    <div className="m-auto w-75 p-3" style={{ border: "2px dashed green" }}>
      <span onClick={(e) => clickToCopy(e)} className="wx__display_1">
        Display 1
      </span>
      <span onClick={(e) => clickToCopy(e)} className="wx__display_2">
        Display 2
      </span>
      <span onClick={(e) => clickToCopy(e)} className="wx__display_3">
        Display 3
      </span>
      <h4 className="mt-1 text-danger bg-dark">Headline</h4>
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_h1 wx__text_semibold"
      >
        Headline 1/Semibold
      </span>
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_h2 wx__text_semibold"
      >
        Headline 2/Semibold
      </span>
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_h3 wx__text_semibold"
      >
        Headline 3/Semibold
      </span>
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_h3 wx__text_regular"
      >
        Headline 3/Regular
      </span>
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_h4 wx__text_regular"
      >
        Headline 4/Regular
      </span>
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_h4 wx__text_medium"
      >
        Headline 4/Medium
      </span>
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__h5_text wx__text_semibold"
      >
        Headline 5/Semibold
      </span>
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_h6 wx__text_regular"
      >
        Headline 6/Regular
      </span>
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_h6 wx__text_medium"
      >
        Headline 6/Medium
      </span>
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_h6 wx__text_semibold"
      >
        Headline 6/Semibold
      </span>
      <h4 className="mt-1 text-danger  bg-dark">Subtitle</h4>
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_subtitle wx__text_regular"
      >
        Subtitle Regular
      </span>
      <br />
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_subtitle wx__text_medium"
      >
        Subtitle Medium
      </span>
      <br />
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_subtitle wx__text_semibold"
      >
        Subtitle Semibold
      </span>
      <h4 className="mt-1 text-danger  bg-dark">Body</h4>
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_body wx__text_regular"
      >
        Body /Regular
      </span>
      <br />
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_body wx__text_medium"
      >
        Body /Medium
      </span>
      <br />
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_body wx__text_strong"
      >
        Body /Strong
      </span>
      <br />
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_body wx__text_underline"
      >
        Body /Underline
      </span>
      <br />
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_body wx__text_strikethrough"
      >
        Body /Strikethrough
      </span>
      <br />
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_body wx__text_italic"
      >
        Body /Italic
      </span>
      <br />
      <span onClick={(e) => clickToCopy(e)} className="wx__text_body_code">
        Body /Code
      </span>
      <br />
      {/* <h1  onClick={(e) => clickToCopy(e)} className="wx__footnote_desc text-block">Footnote description</h1> */}
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_small wx__text_medium "
      >
        Small/Medium
      </span>
      <br />
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_small wx__text_regular "
      >
        Small/Regular
      </span>
      <br />
      <span onClick={(e) => clickToCopy(e)} className="wx__text_caption">
        caption
      </span>
      <h4 className="mt-1 text-danger  bg-dark">Button</h4>
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_btn_big wx__text_semibold "
      >
        Button/Big
      </span>
      <br />
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_btn_regular wx__text_semibold"
      >
        Button/Regular
      </span>
      <br />
      <span
        onClick={(e) => clickToCopy(e)}
        className="wx__text_btn_small wx__text_regular "
      >
        Button/Small
      </span>
      {/* <button onClick={handleLogin}>Login</button> */}
      {/* <button onClick={handleRegister}>Register</button> */}
    </div>
  );
};

export default Font;
