import React from "react";
declare namespace JSX {
  interface IntrinsicElements {
    div: { disabled?: string };
  }
}

const Inputs = () => {
  return (
    <div>
      <form action="">
        <div className="row p-3">
          <div className="col">
            <div style={{ width: "308px" }}>
              <input disabled className="wx__input_primary " type="text" />
              <input disabled className="wx__input_secondary  " type="text" />
              <input disabled className="wx__input_warning " type="text" />
            </div>
            <iframe
              title="input filed iframe"
              width="100%"
              height="300"
              src="//jsfiddle.net/TituX/s3bL6rqo/6/embedded/html/"
              allowFullScreen={true}
              frameBorder="0"
            ></iframe>
          </div>
          <div className="col">
            <div style={{ width: "308px" }}>
              <input disabled className="wx__input_success " type="text" />
              <input disabled className="wx__input_danger " type="text" />
              <input
                style={{ visibility: "hidden" }}
                className="wx__input_danger "
                type="text"
              />
            </div>
            <iframe
              title="input filed iframe"
              width="100%"
              height="300"
              src="//jsfiddle.net/TituX/s3bL6rqo/8/embedded/html/"
              allowFullScreen={true}
              frameBorder="0"
            ></iframe>
          </div>
        </div>
        <div className="row p-3">
          <div className="col">
            <div style={{ width: "308px" }}>
              <input
                placeholder="Text Here"
                className="wx__input_primary "
                type="text"
                disabled
              />
              <input
                placeholder="Text Here"
                className="wx__input_secondary  "
                type="text"
                disabled
              />
              <input
                placeholder="Text Here"
                className="wx__input_warning "
                type="text"
                disabled
              />
            </div>
          </div>
          <div className="col">
            <div style={{ width: "308px" }}>
              <input
                placeholder="Text Here"
                className="wx__input_success "
                type="text"
              />
              <input
                placeholder="Text Here"
                className="wx__input_danger "
                type="text"
              />
            </div>
          </div>
        </div>
        <div className="row p-3">
          <div className="col">
            <div style={{ width: "308px" }}>
              <div aria-disabled className="wx__input_group_primary ">
                <span className="material-icons-outlined">person</span>
                <input
                  placeholder="Text Here"
                  className="wx__input_primary"
                  type="text"
                />
              </div>
              <div aria-disabled={true} className="wx__input_group_secondary ">
                <input
                  placeholder="Text Here"
                  className="wx__input_secondary"
                  type="text"
                />
                <span className="material-icons-outlined">person</span>
              </div>
              <div aria-disabled={false} className="wx__input_group_warning ">
                <span className="material-icons-outlined">person</span>
                <input
                  placeholder="Text Here"
                  className="wx__input_warning"
                  type="text"
                />
                <span className="material-icons-outlined">person</span>
              </div>
            </div>
            <iframe
              title="input all style"
              width="100%"
              height="300"
              src="//jsfiddle.net/TituX/s3bL6rqo/11/embedded/html/"
              allowFullScreen={true}
              frameBorder="0"
            ></iframe>
          </div>
          <div className="col">
            <div style={{ width: "308px" }}>
              <div className="wx__input_group_success ">
                <span className="material-icons-outlined">person</span>
                <input
                  placeholder="Text Here"
                  className="wx__input_success"
                  type="text"
                />
                <span className="material-icons-outlined">person</span>
              </div>
              <div className="wx__input_group_danger ">
                <span className="material-icons-outlined">person</span>
                <input
                  placeholder="Text Here"
                  className="wx__input_danger "
                  type="text"
                />
                <span className="material-icons-outlined">person</span>
              </div>
              <div
                style={{ visibility: "hidden" }}
                className="wx__input_group_danger "
              >
                <span className="material-icons-outlined">person</span>
                <input
                  placeholder="Text Here"
                  className="wx__input_danger "
                  type="text"
                />
                <span className="material-icons-outlined">person</span>
              </div>
            </div>
            <iframe
              title="input all style"
              width="100%"
              height="300"
              src="//jsfiddle.net/TituX/s3bL6rqo/12/embedded/html/"
              allowFullScreen={true}
              frameBorder="0"
            ></iframe>
          </div>
        </div>
        <div className="row w-50 m-auto ">
          <div className="wx__form_group">
            <label htmlFor="">
              Selling Price <span>*</span>
            </label>
            <div className="wx__input_group_danger ">
              <span className="material-icons-outlined">person</span>
              <input
                placeholder="Text Here"
                className="wx__input_danger "
                type="text"
              />
              <span className="material-icons-outlined">person</span>
            </div>
            <span className="note_text">Note</span>
          </div>
          <iframe
            title="something"
            width="100%"
            height="300"
            src="//jsfiddle.net/TituX/prew9147/embedded/html/"
            allowFullScreen={true}
            frameBorder="0"
          ></iframe>
        </div>
        <div className="row w-50 mx-auto my-5 ">
          <div aria-disabled={true} className="wx__form_group">
            <label htmlFor="">
              Selling Price <span>*</span>
            </label>
            <input
              placeholder="Text Here"
              className="wx__input_danger "
              type="text"
            />
            <span className="note_text">Note</span>
          </div>
          <iframe
            title="nothing"
            width="100%"
            height="300"
            src="//jsfiddle.net/TituX/prew9147/1/embedded/html/"
            allowFullScreen={true}
            frameBorder="0"
          ></iframe>
        </div>
      </form>
    </div>
  );
};

export default Inputs;
