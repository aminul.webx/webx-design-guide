import React from "react";
import "../../sass/bootstrap/forms/switch.scss";

const Switch = () => {
  return (
    <div>
      <div className="row">
        <div className="col">
          <div className="form-check form-check-inline">
            <input
              className="form-check-input"
              type="radio"
              name="inlineRadioOptions"
              id="inlineRadio1"
              value="option1"
            />
            <label className="form-check-label" htmlFor="inlineRadio1">
              Radio - Inactive
            </label>
          </div>
          <div className="form-check form-check-inline">
            <input
              className="form-check-input"
              type="radio"
              name="inlineRadioOptions"
              id="inlineRadio2"
              value="option2"
            />
            <label className="form-check-label" htmlFor="inlineRadio2">
              Radio
            </label>
          </div>
          <div className="form-check form-check-inline">
            <input
              className="form-check-input"
              type="radio"
              name="inlineRadioOptions"
              id="inlineRadio3"
              value="option3"
              disabled
            />
            <label className="form-check-label" htmlFor="inlineRadio3">
              Radio
            </label>
          </div>
          <div className="col">
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox1"
                value="option1"
              />
              <label className="form-check-label" htmlFor="inlineCheckbox1">
                Checkpoint
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="option2"
              />
              <label className="form-check-label" htmlFor="inlineCheckbox2">
                Checkpoint
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox3"
                value="option3"
                disabled
              />
              <label className="form-check-label" htmlFor="inlineCheckbox3">
                Checkpoint
              </label>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="form-check form-switch  w-75 d-flex justify-content-between">
            <label
              className="form-check-label"
              htmlFor="flexSwitchCheckDefault"
            >
              Default switch checkbox input
            </label>
            <div className="form-wrapper">
              <input
                className="form-check-input"
                type="checkbox"
                id="flexSwitchCheckDefault"
                checked-value="Unlimited"
                unchecked-value="Limited"
              />
            </div>
          </div>
          <div className="form-check form-switch  w-75 d-flex justify-content-between">
            <label
              className="form-check-label"
              htmlFor="flexSwitchCheckChecked"
            >
              Checked switch checkbox input
            </label>
            <input
              className="form-check-input"
              type="checkbox"
              id="flexSwitchCheckChecked"
              defaultChecked
            />
          </div>
          <div className="form-check form-switch w-75 d-flex justify-content-between">
            <label
              className="form-check-label"
              htmlFor="flexSwitchCheckDisabled"
            >
              Disabled switch checkbox input
            </label>
            <input
              className="form-check-input"
              type="checkbox"
              id="flexSwitchCheckDisabled"
              disabled
            />
          </div>
          <div className="form-check form-switch w-75 d-flex justify-content-between">
            <label
              className="form-check-label"
              htmlFor="flexSwitchCheckCheckedDisabled"
            >
              Disabled checked switch checkbox input
            </label>
            <input
              className="form-check-input"
              type="checkbox"
              id="flexSwitchCheckCheckedDisabled"
              defaultChecked
              disabled
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Switch;
