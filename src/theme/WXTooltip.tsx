import React from "react";
import "../sass/custom/wxtooltip.scss";

const WXTooltip = ({
  children,
  place = "top",
  message = "tooltip on top",
}: any) => {
  var pointerX = -1;
  var pointerY = -1;
  document.onmousemove = function (event) {
    pointerX = event.pageX;
    pointerY = event.pageY;
  };

  setInterval(pointerCheck, 1000);
  function pointerCheck() {
    // console.log("Cursor at: " + pointerX + ", pointer y" + pointerY);
    // console.log("Window Height" + window.screen.height, "pointer" + pointerY);
    // console.log("Window Width", window.screen.width, "pointer", pointerX);
  }

  const tooltipInteraction = (e: React.ChangeEvent<HTMLInputElement>) => {
    // const element = document.querySelector(".wx__tooltip");

    const element = e.target;

    // const elementBody = element.firstChild;

    let elementBody: any;
    if (element.classList[0] === "wx__tooltip") {
      elementBody = e.target.firstChild;
      console.log("ok");
    } else {
      elementBody = e.target.parentNode;
      console.log("not ok");
    }

    console.log(element);

    const elementBodyPosition = elementBody.getBoundingClientRect();
    // The rect has all the data we want
    // console.log(element.firstChild);

    elementBody.classList.add("wx__tooltip__element__body");

    const windowHeight = window.innerHeight;
    const windowWidth = window.innerWidth;
    const elementBodyTop = elementBodyPosition.top;
    const elementBodyLeft = elementBodyPosition.left;
    const elementBodyHeight = elementBody.offsetHeight;
    const elementBodyWidth = elementBody.offsetWidth;
    const tooltipMessage: any = document.getElementById(`wxTooltipMessage`);
    const tooltipHeight = tooltipMessage.offsetHeight + 10;
    const tooltipWidth = tooltipMessage.offsetWidth + 10;

    const bottom = windowHeight - (elementBodyTop + elementBodyHeight);
    const right = windowWidth - (elementBodyLeft + elementBodyWidth);

    console.log({
      windowHeight: windowHeight,
      windowWidth: windowWidth,
      elementBodyTop: elementBodyTop,
      elementBodyLeft: elementBodyLeft,
      elementBodyHeight: elementBodyHeight,
      elementBodyWidth: elementBodyWidth,
      tooltipHeight: tooltipHeight,
      tooltipWidth: tooltipWidth,
      right: right,
    });

    // const leftValue = () => {
    //   tooltipMessage.style.left = elementBodyLeft - tooltipWidth + "px";
    // };

    const topBottomValue = () => {
      tooltipMessage.style.left = elementBodyLeft - 17 + "px";
      // elementBodyLeft - 17 + elementBodyWidth / 2 + "px";
    };

    if (
      "wx__tooltip__top" === tooltipMessage.classList[0] ||
      "wx__tooltip__bottom" === tooltipMessage.classList[0]
    ) {
      tooltipMessage.style.left = elementBodyLeft - 17 + "px";
    } else if ("wx__tooltip__left" === tooltipMessage.classList[0]) {
      tooltipMessage.style.left = elementBodyLeft - tooltipWidth + "px";
    } else if ("wx__tooltip__right" === tooltipMessage.classList[0]) {
      tooltipMessage.style.left = elementBodyLeft + tooltipWidth - 30 + "px";
    }

    // const topTooltip = document.querySelector(".wx__tooltip__top");
    // const bottomTooltip = document.querySelector(".wx__tooltip__bottom");
    // const leftTooltip = document.querySelector(".wx__tooltip__left");
    // const rightTooltip = document.querySelector(".wx__tooltip__right");

    switch (place) {
      case "top":
        if ((tooltipHeight && tooltipWidth) <= elementBodyTop) {
          tooltipMessage.removeAttribute("class");
          tooltipMessage.classList.add("wx__tooltip__top");
          // topBottomValue();
          console.log("its coming from top 1st");
        } else if (
          (tooltipHeight && tooltipWidth) <=
          windowHeight - elementBodyTop
        ) {
          tooltipMessage.removeAttribute("class");
          tooltipMessage.classList.add("wx__tooltip__bottom");
          console.log("its coming from top 2nd");
        } else if ((tooltipHeight && tooltipWidth) <= elementBodyLeft) {
          tooltipMessage.removeAttribute("class");
          tooltipMessage.classList.add("wx__tooltip__left");
          console.log("its coming from top 3rd");
          // leftValue();
        } else {
          tooltipMessage.removeAttribute("class");
          tooltipMessage.classList.add("wx__tooltip__right");
          console.log("its coming from top 4th");
        }
        break;
      case "bottom":
        if ((tooltipHeight && tooltipWidth) <= windowHeight - elementBodyTop) {
          tooltipMessage.removeAttribute("class");
          tooltipMessage.classList.add("wx__tooltip__bottom");
          console.log("its coming from bottom 1st");
        } else if ((tooltipHeight && tooltipWidth) <= elementBodyTop) {
          //   tooltipMessage.removeAttribute("class");
          tooltipMessage.removeAttribute("class");
          tooltipMessage.classList.add("wx__tooltip__top");
          console.log("its coming from bottom 2nd");
        } else if ((tooltipHeight && tooltipWidth) <= elementBodyLeft) {
          tooltipMessage.removeAttribute("class");
          tooltipMessage.classList.add("wx__tooltip__left");
          console.log("its coming from bottom 3rd");
          // leftValue();
        } else {
          tooltipMessage.removeAttribute("class");
          tooltipMessage.classList.add("wx__tooltip__right");
          console.log("its coming from bottom 4th");
        }
        break;
      case "left":
        if ((tooltipHeight && tooltipWidth) <= elementBodyLeft) {
          tooltipMessage.removeAttribute("class");
          tooltipMessage.classList.add("wx__tooltip__left");
          // leftValue();
          console.log("its coming from left 1st");
        } else if ((tooltipHeight && tooltipWidth) <= right) {
          tooltipMessage.removeAttribute("class");
          tooltipMessage.classList.add("wx__tooltip__right");
          console.log("its coming from left 2nd");
        } else if ((tooltipHeight && tooltipWidth) <= elementBodyTop) {
          //   tooltipMessage.removeAttribute("class");
          tooltipMessage.removeAttribute("class");
          tooltipMessage.classList.add("wx__tooltip__top");
          console.log("its coming from left 3rd");
        } else {
          tooltipMessage.removeAttribute("class");
          tooltipMessage.classList.add("wx__tooltip__bottom");
          console.log("its coming from left 4th");
        }
        break;
      case "right":
        if ((tooltipHeight && tooltipWidth) <= right) {
          tooltipMessage.removeAttribute("class");
          tooltipMessage.classList.add("wx__tooltip__right");
          console.log("its coming from right 1st");
        } else if ((tooltipHeight && tooltipWidth) <= elementBodyLeft) {
          tooltipMessage.removeAttribute("class");
          tooltipMessage.classList.add("wx__tooltip__left");
          console.log("its coming from right 2nd");
          // leftValue();
        } else if ((tooltipHeight && tooltipWidth) <= elementBodyTop) {
          //   tooltipMessage.removeAttribute("class");
          tooltipMessage.removeAttribute("class");
          tooltipMessage.classList.add("wx__tooltip__top");
          console.log("its coming from right 3rd");
        } else {
          tooltipMessage.removeAttribute("class");
          tooltipMessage.classList.add("wx__tooltip__bottom");
          console.log("its coming from right 4th");
        }
        break;
      default:
        break;
    }
  };

  //   console.log(children);

  const onMouseLeaveHandler = () => {
    // const tooltipMessage = document.getElementById(`wxTooltipMessage`);
    // tooltipMessage.removeAttribute("class");
    // tooltipMessage.classList.add(`wx__tooltip__${place}`);
  };

  return (
    <div
      onMouseMove={(e: React.ChangeEvent<any>) => tooltipInteraction(e)}
      onMouseLeave={onMouseLeaveHandler}
      className="wx__tooltip"
    >
      {children}
      <span id="wxTooltipMessage" className={`wx__tooltip__${place}`}>
        {message}
      </span>
    </div>
  );
};

export default WXTooltip;
