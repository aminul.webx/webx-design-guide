import React, { useEffect, useState } from "react";
import "../sass/custom/table.scss";
import WXPagination from "./WXPagination";

const Table = () => {
  const [currentPage, setCurrentPage] = useState<number>(5);
  const [testCurrentPage, setTestCurrentPage] = useState(8);

  useEffect(() => {
    const copy = document.querySelectorAll(".wx__table_text_icon");
    // console.log(copy.offsetX);
  }, []);
  const onChangePage = (nextPage: any) => {
    setCurrentPage(nextPage);
    console.log(nextPage);
  };
  const onTestChangePage = (nextPage: any) => {
    setTestCurrentPage(nextPage);
    console.log(nextPage);
  };

  return (
    <div>
      <table>
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Handle</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td className="wx__table_cell_text_with_icon">
              Abdullah Al Noman
              <span className="material-icons-round copy_icon">
                content_copy
              </span>
            </td>
            <td className="wx__table_cell_money">৳1234</td>
            <td className="wx__table_cell_date">12 Feb 2022</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td className="wx__table_cell_focus_text_with_sub">
              Abdullah Al Noman
              <span className="table_subtext">Date added 12 May 2021</span>
            </td>
            <td className="">
              <div className="wx__table_cell_phone">
                <span className="material-icons-round phone_icon">phone</span>
                +88016710-365-459
              </div>
            </td>
            <td className="wx__table_cell_empty">@fat</td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>
              <div className="wx__table_cell_focus_text_with_tags">
                Abdullah Al Noman
                <button className="wx__btn_tags_primary wx__fixed_tags ms-3">
                  Primary
                </button>
              </div>
            </td>
            <td className="wx__table_cell_multi-lines">
              <span>
                Mens, Formal Shirt, Casual Shirt,Women Casual Shirt, Women
                Formal
              </span>
            </td>
            <td className="">
              {/* <span>1</span>
              <div>
                <span className="material-icons-round">arrow_drop_up</span>
                <span className="material-icons-round">arrow_drop_down</span>
              </div> */}
              <div className="wx__table_cell_input_number">
                <input type="number" defaultValue={1} name="" id="" />
                <div className=" wx__plus_minus_btn">
                  <span
                    onClick={(e: any) =>
                      e.target.parentNode.parentNode
                        .querySelector("input[type=number]")
                        .stepUp()
                    }
                    className="material-icons-round plus"
                  >
                    arrow_drop_up
                  </span>
                  <span
                    onClick={(e: any) =>
                      e.target.parentNode.parentNode
                        .querySelector("input[type=number]")
                        .stepDown()
                    }
                    className="material-icons-round minus"
                  >
                    arrow_drop_down
                  </span>
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>
              <div className="wx__table_cell_link_with_subs">
                <a href="#">Festival classic shirt</a>
                <span className="table_subtext">
                  <span>Large</span>
                  <span>Unit Price ৳1123.23</span>
                </span>
              </div>
            </td>
            <td className="wx__table_cell_input_edit">
              10
              <span className="material-icons-round">edit</span>
            </td>
            <td className="">
              <div className="wx__table_cell_tags">
                <button className="wx__btn_tags_warning wx__fixed_tags">
                  Warning
                </button>
                <span className="material-icons-round">arrow_drop_down</span>
              </div>
            </td>
          </tr>
        </tbody>
      </table>

      <div>
        <WXPagination
          totalCount={300}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          onPageChange={onChangePage}
        />
      </div>
      <div>
        {/* <WXPagination
          totalCount={150}
          currentPage={testCurrentPage}
          onPageChange={onTestChangePage}
        /> */}
      </div>
    </div>
  );
};

export default Table;
