import React from "react";
import ReactDOM from "react-dom";
import "../sass/custom/toast.scss";

// this is main functisn of toast slice. inside the function all the functionallity will be added and this function will specify every single toast
export const wxToastSlice = (
  message = "🦄 Wow so easy !",
  style: any = {
    position: "",
  }
) => {
  // for define the icon according to toast type
  const typeIcon = () => {
    switch (style.type) {
      case "success":
        return (
          <span className="material-icons-round wx__single__toast__front__icon">
            check_circle
          </span>
        );
      case "danger":
        return (
          <span className="material-icons-round wx__single__toast__front__icon">
            cancel1
          </span>
        );
      case "warning":
        return (
          <span className="material-icons-round wx__single__toast__front__icon">
            warning
          </span>
        );
      default:
        return <></>;
    }
  };

  // this function is created for append a full slice of div inside a wrapper div when initially everything is created
  const TestToast = (
    <div
      className={
        style.type ? `wx__single__toast-${style.type}` : "wx__single__toast"
      }
    >
      <div className="wx__single__toast__body">
        {typeIcon()}
        <span className="wx__single__toast__message-part">{message}</span>
      </div>
      <span
        onClick={(e: any) => e.target.parentNode.remove()}
        className="material-icons-round wx__single__toast__close__btn"
      >
        close
      </span>
    </div>
  );

  // this function is crated for append the html inside a toast div
  const tostContent = (
    <>
      <div className="wx__single__toast__body">
        {typeIcon()}
        <span className="wx__single__toast__message-part">{message}</span>
      </div>
      <span
        onClick={(e: any) => {
          e.target.parentNode.remove();
        }}
        className="material-icons-round wx__single__toast__close__btn"
      >
        close
      </span>
    </>
  );
  let document: any;

  // everytime when someone call an event for toast then these two div will create . one is for wrapper (that will describe the position of toast and other div will create for every single toast)
  const div = document.createElement("div");
  const div2 = document.createElement("div");

  const wxToastWrapper = document.querySelector(
    style.position
      ? `.wx__toast__wrapper-${style.position}`
      : ".wx__toast__wrapper"
  );

  if (!wxToastWrapper) {
    // div.classList.add("wx__toast__wrapper");
    style.position
      ? div.classList.add(`wx__toast__wrapper-${style.position}`)
      : div.classList.add("wx__toast__wrapper");
    document.querySelector(".wx__toast").appendChild(div);
    ReactDOM.render(TestToast, div);
  } else {
    // const div = document.createElement("div");
    style.type
      ? div2.classList.add(`wx__single__toast-${style.type}`)
      : div2.classList.add(`wx__single__toast`);
    style.position
      ? document
          .querySelector(`.wx__toast__wrapper-${style.position}`)
          .appendChild(div2)
      : document.querySelector(".wx__toast__wrapper").appendChild(div2);
    // document.querySelector(".wx__toast__wrapper").appendChild(div2);
    console.log("ok");
    ReactDOM.render(tostContent, div2);
    ReactDOM.render(div2, div);
  }

  // using switch statement for checking the variation
  // switch (style.position) {
  //   case "top-left":
  //     const wxToastWrapper = document.querySelector(".wx__toast__wrapper");

  //     if (!wxToastWrapper) {
  //       div.classList.add("wx__toast__wrapper");
  //       document.querySelector(".wx__toast").appendChild(div);
  //       ReactDOM.render(TestToast, div);
  //     } else {
  //       // const div = document.createElement("div");
  //       div2.classList.add("wx__single__toast__colored");
  //       document.querySelector(".wx__toast__wrapper").appendChild(div2);
  //       console.log("ok");
  //       ReactDOM.render(tostContent, div2);
  //       ReactDOM.render(div2, div);
  //     }
  //     break;
  //   case "top-center":
  //     console.log("top center");
  //     break;
  //   case "top-right":
  //     console.log("top right");
  //     break;
  //   case "bottom-left":
  //     console.log("bottom left");
  //     break;
  //   case "bottom-center":
  //     console.log("bottom center");
  //     break;
  //   case "bottom-right":
  //     break;
  //   default:
  //     console.log("bottom center default");
  //     break;
  // }
};

// this is the main div for toast . inside this div all toast and wrapper will appear functionally
const WXToast = () => {
  // inside this div every toast will appear
  return <div className="wx__toast"></div>;
};

export default WXToast;
