import "bootstrap/dist/css/bootstrap.min.css";
// import { Tooltip } from "bootstrap/dist/js/bootstrap.esm.min.js";
import "bootstrap/dist/js/bootstrap.min.js";
import React from "react";
import { Button, OverlayTrigger, Tooltip } from "react-bootstrap";

const BootTooltip = () => {
  //   var tooltipTriggerList = [].slice.call(
  //     document.querySelectorAll('[data-bs-toggle="tooltip"]')
  //   );
  //   var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  //     return new bootstrap.Tooltip(tooltipTriggerEl);
  //   });

  //   useEffect(() => {
  //     //init tooltip
  //     Array.from(
  //       document.querySelectorAll('button[data-bs-toggle="tooltip"]')
  //     ).forEach((tooltipNode) => new Tooltip(tooltipNode));
  //   });

  return (
    <div>
      <button
        type="button"
        className="btn btn-secondary"
        data-bs-toggle="tooltip"
        data-bs-placement="top"
        title="Tooltip on top"
      >
        Tooltip on top
      </button>
      <button
        type="button"
        className="btn btn-secondary"
        data-bs-toggle="tooltip"
        data-bs-placement="right"
        title="Tooltip on right"
      >
        Tooltip on right
      </button>
      <button
        type="button"
        className="btn btn-secondary"
        data-bs-toggle="tooltip"
        data-bs-placement="bottom"
        title="Tooltip on bottom"
      >
        Tooltip on bottom
      </button>
      <button
        type="button"
        className="btn btn-secondary"
        data-bs-toggle="tooltip"
        data-bs-placement="left"
        title="Tooltip on left"
      >
        Tooltip on left
      </button>
      <div>/////////////////////////////////////////////</div>
      {["top", "right", "bottom", "left"].map((placement) => (
        <OverlayTrigger
          key={placement}
          //   placement={placement}
          placement="auto"
          overlay={
            <Tooltip id={`tooltip-${placement}`}>
              Tooltip on <strong>{placement}</strong>.
            </Tooltip>
          }
        >
          <Button variant="secondary">Tooltip on {placement}</Button>
        </OverlayTrigger>
      ))}
    </div>
  );
};

export default BootTooltip;
