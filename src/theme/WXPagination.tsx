import React, { useEffect, useState } from "react";
import "../sass/custom/wxpagination.scss";

interface ITablePagination {
  totalCount: number;
  currentPage: number;
  setCurrentPage?: any;
  onPageChange: any;
}

const TablePagination = ({
  totalCount,
  currentPage,
  setCurrentPage,
  onPageChange,
}: ITablePagination) => {
  const [perPages, setPerPages] = useState([10, 20, 30, 40, 50, 100]);

  const [pages, setPages] = useState([1]);

  const [resultPerPage, setResultPerPage] = useState(10);

  const showingFrom = resultPerPage * currentPage - (resultPerPage - 1);

  const showingTo = resultPerPage * currentPage;

  useEffect(() => {
    // for initial page calculation
    const total = Math.ceil(totalCount / resultPerPage);
    calculatePages(total);
  }, [currentPage, resultPerPage, totalCount]);

  // function for perpage change
  const onPerPageChange = (pageNumber: number) => {
    const total = Math.ceil(totalCount / pageNumber);
    calculatePages(total);
    setResultPerPage(pageNumber);
    setCurrentPage(1);
  };

  // calculating the pages on per pages changes
  const calculatePages = (total: number) => {
    let pp = [];
    for (let p = 1; p <= total; p++) {
      pp.push(p);
    }
    setPages(pp);
  };

  // this function is defining the first number of slice
  const firstIndexOfSlice = (): number => {
    // TODO:: need to optimization and recheck before use
    // return pages.length <= 4
    //   ? 0
    //   : currentPage === pages.length
    //   ? currentPage - 5
    //   : currentPage === pages.length - 1
    //   ? currentPage - 4
    //   : currentPage <= 2
    //   ? 0
    //   : currentPage - 3;
    return pages.length <= 4
      ? 0
      : currentPage === pages.length
      ? currentPage - 3
      : currentPage === pages.length - 1
      ? currentPage - 2
      : currentPage <= 2 || currentPage <= 3
      ? 0
      : currentPage - 2;
    // return currentPage <= 3 ? 0 : currentPage - 2;
  };

  // this function is defining the last number of slice
  const lastIndexOfSlice = (): number => {
    // TODO:: need to optimization and recheck before use
    // return pages.length <= 4 || currentPage <= 1
    //   ? currentPage + 4
    //   : currentPage <= 2
    //   ? currentPage + 3
    //   : pages.length < currentPage
    //   ? (currentPage = 1)
    //   : currentPage + 2;
    return pages.length <= 4 || currentPage <= 1
      ? currentPage + 4
      : currentPage <= 2
      ? currentPage + 3
      : currentPage <= 3
      ? currentPage + 2
      : pages.length < currentPage
      ? (currentPage = 1)
      : currentPage + 1;
  };

  return (
    <div className="wx__table_pagination_main">
      {/* table per page number selection start */}
      <div className="wx__perpage_data">
        <p>Table Pagination</p>
        <select onChange={(e: any) => onPerPageChange(e.target.value)}>
          {perPages.length
            ? perPages.map((page, index) => {
                return (
                  <option value={page} key={index}>
                    {page}
                  </option>
                );
              })
            : null}
        </select>
      </div>
      {/* table per page number selection end */}

      <div className="wx__table_pagination">
        <span className="wx__pagination_text">
          Showing {showingFrom}-{showingTo} of {totalCount}
        </span>

        <div className="wx__pagination_div">
          <ul className="wx__ul">
            {/* paginatin left button start */}
            <li
              onClick={() => {
                currentPage > 1 && onPageChange(1);
              }}
              className={`wx__table_single_li pagination_btn left_btn ${
                (currentPage === 1 || pages.length === 0) && "disable-page"
              }`}
            >
              <span className="material-icons-round span">first_page</span>
            </li>
            <li
              onClick={() => {
                currentPage > 1 && onPageChange(currentPage - 1);
              }}
              className={`wx__table_single_li pagination_btn left_btn ${
                (currentPage === 1 || pages.length === 0) && "disable-page"
              }`}
            >
              <span className="material-icons-round span">chevron_left</span>
            </li>
            {/* paginatin left button end */}

            {pages.length >= 5 && currentPage > 3 && (
              <li className="wx__table_single_li ">...</li>
            )}

            {/* main div for dynamic page number start*/}
            <div className="page_number">
              {pages
                .slice(firstIndexOfSlice(), lastIndexOfSlice())
                .map((page, index) => {
                  return (
                    <li
                      className={`wx__table_single_li 
                    ${currentPage === page ? "active-page" : ""}
                  `}
                      key={index}
                      onClick={() => {
                        onPageChange(page);
                      }}
                    >
                      {page}
                    </li>
                  );
                })}
            </div>
            {/* main div for dynamic page number end*/}

            {pages.length >= 5 && currentPage < pages.length - 2 && (
              <li className="wx__table_single_li ">...</li>
            )}

            {/* paginatin right button start */}

            <li
              onClick={() => {
                currentPage < pages.length && onPageChange(currentPage + 1);
              }}
              className={`wx__table_single_li pagination_btn right_btn ${
                (currentPage === pages.length || pages.length === 0) &&
                "disable-page"
              }`}
            >
              <span className="material-icons-round span">chevron_right</span>
            </li>

            <li
              onClick={() => {
                currentPage < pages.length && onPageChange(pages.length);
              }}
              className={`wx__table_single_li pagination_btn right_btn ${
                (currentPage === pages.length || pages.length === 0) &&
                "disable-page"
              }`}
            >
              <span className="material-icons-round  span">last_page</span>
            </li>
            {/* paginatin left button end */}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default TablePagination;
