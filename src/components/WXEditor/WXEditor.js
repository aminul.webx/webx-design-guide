import { Editor } from "@tinymce/tinymce-react";
import React, { useRef } from "react";
// Toolbar icons
import "./WXEditor.scss";

const WXEditor = () => {
  const editorRef = useRef(null);
  const log = () => {
    if (editorRef.current) {
      console.log(editorRef.current.getContent());
    }
  };

  return (
    <div style={{ width: "50vw" }}>
      <Editor
        apiKey="7e7t39rksklm5fdtlncmuc0kwouu690su0jwtpqsiafq4cxp"
        onInit={(evt, editor) => (editorRef.current = editor)}
        initialValue="<p>This is the initial content of the editor.</p>"
        init={{
          height: 228,
          width: 606,
          menubar: false,
          selector: "textarea",
          plugins: [
            "advlist",
            "autolink",
            "lists",
            "link",
            "image",
            "visualchars",
            // "charmap",
            "anchor",
            "searchreplace",
            "visualblocks",
            "code",
            "image imagetools",
            // "fullscreen",
            "insertdatetime",
            "media",
            "table",
            "preview",
            "help",
            // "wordcount",
          ],
          toolbar:
            "undo redo | blocks | " +
            "bold italic forecolor | alignleft aligncenter " +
            "image imagetools | visualchars " +
            "alignright alignjustify | bullist numlist outdent indent | " +
            "removeformat | help",
          content_style:
            "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
        }}
      />
      <button onClick={log}>Log editor content</button>
    </div>
  );
};

export default WXEditor;
// 7e7t39rksklm5fdtlncmuc0kwouu690su0jwtpqsiafq4cxp
