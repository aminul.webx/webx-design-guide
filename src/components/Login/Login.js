import React from "react";
import "./Login.scss";

const Login = () => {
  return (
    <div className="login-main mt-4">
      <div className="text-center">
        <h1 className="login__getstarted__text">Welcome Back</h1>
        <p className="already__have__account__text">
          Dont't Have An Account?
          <span className="login__text"> Create Account</span>
        </p>
      </div>
      <div>
        <form action="" className="d-flex flex-column">
          <div className="d-flex flex-column mt-md-3 mt-sm-2">
            <label htmlFor="" className="login__label__text mb-2">
              Email address or phone number
            </label>
            <input type="text" className="login__input" />
          </div>
          <div className="d-flex flex-column mt-md-3 mt-sm-2">
            <div className="d-flex flex-row justify-content-between">
              <label htmlFor="" className="login__label__text mb-2">
                Password
              </label>
              <p className="forgot__password__text">Forgot Password?</p>
            </div>
            <input type="text" className="login__input" />
          </div>
          <button className="create__webx__id__btn py-2">Log In</button>
        </form>
      </div>
      <div></div>
    </div>
  );
};

export default Login;
