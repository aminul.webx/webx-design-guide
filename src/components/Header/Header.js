import React from "react";
import WebXLogo from "../../assets/icons/webx-logo.svg";
import Ellipse from "../../assets/images/Ellipse1.png";
// import Ellipse from "../../assets/images";
import "./Header.scss";

const Header = () => {
  return (
    <div className="wx__header d-flex justify-content-between">
      <div className="wx__header__logo">
        <img src={WebXLogo} alt="" />
      </div>
      <div className="wx__header__right__part d-flex align-items-center">
        <div className="wx__avatar__img">
          <img src={Ellipse} alt="" />
        </div>
        <div>
          <span className="wx__text_subtitle wx__text_semibold">
            My Account
          </span>
        </div>
      </div>
    </div>
  );
};

export default Header;
