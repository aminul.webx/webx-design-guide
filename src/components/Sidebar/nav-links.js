const navLinks = [
  { label: "Dashboard", link: "/dashboard", icon: "home" },
  { label: "Orders", link: "/orders", icon: "receipt" },
  {
    label: "Products",
    link: "/products",
    icon: "category",
    childrens: [
      { label: "User-A", link: "/configuration/user-a" },
      { label: "User-B", link: "/configuration/user-b" },
      { label: "User-C", link: "/configuration/user-c" },
      { label: "User-D", link: "/configuration/user-d" },
      { label: "User-E", link: "/configuration/user-e" },
    ],
  },
  { label: "Customers", link: "/cutomers", icon: "person" },
  { label: "Sales Promotion", link: "/sales_promotion", icon: "local_offer" },
  { label: "Analytics", link: "/analytics", icon: "bar_chart" },
  {
    label: "Apps",
    link: "/apps",
    icon: "widgets",
    childrens: [
      { label: "Apps-A", link: "/configuration/apps-a" },
      { label: "Apps-B", link: "/configuration/apps-b" },
      { label: "Apps-C", link: "/configuration/apps-c" },
      { label: "Apps-D", link: "/configuration/apps-d" },
      { label: "Apps-E", link: "/configuration/apps-e" },
      { label: "Apps-F", link: "/configuration/apps-f" },
    ],
  },
];

export default navLinks;
