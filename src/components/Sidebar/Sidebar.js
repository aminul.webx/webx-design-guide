import React, { useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import SideNav from "../../theme/SideNav";
import navLinks from "./nav-links.js";
import "./Sidebar.scss";

const Sidebar = () => {
  const location = useLocation();

  const { pathname } = location;

  // console.log(location.pathname);

  const childrenCollapse = (className) => {
    const navChild = document.querySelector(`.${className}`);
    const navChildrens = navChild.querySelectorAll(
      ".wx__single_side_nav_single_childrens"
    );
    if (navChild.style.maxHeight) {
      navChild.style.maxHeight = null;
      navChild.style.overflow = "hidden";
      navChild.style.opacity = 0;
    } else {
      navChild.style.opacity = 1;
      navChild.style.overflow = "unset";
      navChild.style.maxHeight = navChild.scrollHeight + "px";
    }
    // console.log(navChild);
  };
  useEffect(() => {
    const navChild = document.querySelector(".wx__single_side_nav_childrens");
    const navChildrens = navChild.querySelector(".active");
    // console.log(navChildrens);
    if (navChildrens) {
      navChild.style.maxHeight = navChild.scrollHeight + "px";
      navChild.style.overflow = "unset";
      navChild.style.opacity = 1;
    }
  }, []);

  return (
    <div className="wx__sidebar">
      <div className="wx__sidebar__container">
        <SideNav className="wx__sidebar__wrapper">
          {navLinks.map((nav, i) => (
            <>
              <Link
                to={nav.link}
                onClick={() => {
                  nav.childrens &&
                    childrenCollapse(nav.link.slice(1, -1) + "-childrens");
                }}
                className={`wx__single__side__nav ${
                  pathname === nav.link ? "active" : ""
                }`}
              >
                <span className="material-icons-round">{nav.icon}</span>
                <div className="wx__side__navigation__text">
                  <span>{nav.label}</span>
                </div>
              </Link>
              {nav.childrens && (
                <div
                  className={`wx__single_side_nav_childrens ${
                    nav.link.slice(1, -1) + "-childrens"
                  }`}
                >
                  {nav.childrens.map((child) => (
                    <Link
                      to={child.link}
                      className={`wx__single_side_nav_single_childrens ${
                        pathname === child.link ? "active" : ""
                      }`}
                    >
                      {/* <span className="material-icons-round">{child.icon}</span> */}
                      <div className="wx__side__childrens__text">
                        <span>{child.label}</span>
                      </div>
                    </Link>
                  ))}
                </div>
              )}
            </>
          ))}

          {/* <div className="wx__single__side__nav">
            <span className="material-icons-round">home</span>
            <div className="wx__side__navigation__text">
              <span>Dashboard</span>
            </div>
          </div>
          <div className="wx__single__side__nav">
            <span className="material-icons-round">receipt</span>
            <div className="wx__side__navigation__text">
              <span>Orders</span>
            </div>
          </div>
          <div className="wx__single__side__nav">
            <span className="material-icons-round">category</span>
            <div className="wx__side__navigation__text">
              <span>Products</span>
            </div>
          </div>
          <div className="wx__single__side__nav">
            <span className="material-icons-round">person</span>
            <div className="wx__side__navigation__text">
              <span>Customers</span>
            </div>
          </div>
          <div className="wx__single__side__nav">
            <span className="material-icons-round">local_offer</span>
            <div className="wx__side__navigation__text">
              <span>Sales Promotion</span>
            </div>
          </div>
          <div className="wx__single__side__nav">
            <span className="material-icons-round">bar_chart</span>
            <div className="wx__side__navigation__text">
              <span>Analytics</span>
            </div>
          </div>
          <div className="wx__single__side__nav">
            <span className="material-icons-round">widgets</span>
            <div className="wx__side__navigation__text">
              <span>Apps</span>
            </div>
          </div> */}
          <p className="wx__text_caption wx__sales__channels">Sales Channels</p>
          <div className="wx__single__side__nav">
            <span className="material-icons-round">storefront</span>
            <div className="wx__side__navigation__text">
              <span>Online Store</span>
            </div>
          </div>
          <div className="wx__single__side__nav">
            <span className="material-icons-round">point_of_sale</span>
            <div className="wx__side__navigation__text">
              <span>Points of Sale</span>
            </div>
          </div>
        </SideNav>
      </div>
      <div className="wx__sidebar__bottom">
        <div className="wx__single__side__nav">
          <span className="material-icons-round">settings</span>
          <div className="wx__side__navigation__text">
            <span>Settings</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
