import React, { Fragment } from "react";
type Size =
  | "display_1"
  | "display_2"
  | "display_3 "
  | "h1"
  | "h2"
  | "h3"
  | "h4"
  | "h5"
  | "h6"
  | "subtitle"
  | "body"
  | "body_code"
  | "small"
  | "caption"
  | "btn_big"
  | "btn_regular"
  | "btn_small";

type Weight = "semibold" | "medium" | "regular";
type Style = "normal" | "italic";
type Decoration = "unset" | "underline" | "strikethrough";

interface IWxHeading {
  size?: Size;
  weight?: Weight;
  style?: Style;
  decoration?: Decoration;
  children: any;
}

const WxText = ({
  size = "h1",
  weight = "regular",
  style = "normal",
  decoration = "unset",
  children,
}: IWxHeading) => {
  return (
    <Fragment>
      <span
        className={`wx__text_${size} wx__text_${weight} wx__text_${style} wx__text_${decoration}`}
      >
        {children}
      </span>
    </Fragment>
  );
};

export default WxText;
