import React, { useEffect, useState } from "react";
import "./DragNDrop.scss";

interface IDragNDrop {
  imagesData: any;
  setImagesData: any;
}

const DragNDropNew = ({ imagesData, setImagesData }: IDragNDrop) => {
  // console.log(imagesData);

  const [draggedFrom, setDraggedFrom] = useState<number>(0);
  const [draggedTo, setDraggedTo] = useState<number>(0);

  useEffect(() => {
    var dragSrcEl = null;

    function handleDragStart(e) {
      this.style.opacity = "0.4";

      // dragSrcEl = this;

      // e.dataTransfer.effectAllowed = "move";
      // e.dataTransfer.setData("text/html", this.innerHTML);

      setDraggedFrom(this.getAttribute("data-index"));
    }

    function handleDragOver(e) {
      if (e.preventDefault) {
        e.preventDefault();
      }

      e.dataTransfer.dropEffect = "move";

      return false;
    }

    function handleDragEnter(e) {
      this.classList.add("over");
    }

    function handleDragLeave(e) {
      this.classList.remove("over");
    }

    function handleDrop(e) {
      if (e.stopPropagation) {
        e.stopPropagation(); // stops the browser from redirecting.
      }

      if (dragSrcEl != this) {
        // dragSrcEl.innerHTML = this.innerHTML;
        // this.innerHTML = e.dataTransfer.getData("text/html");
        setDraggedTo(this.getAttribute("data-index"));
      }

      return false;
    }

    function handleDragEnd(e) {
      this.style.opacity = "1";

      items.forEach(function (item) {
        item.classList.remove("over");
      });
    }
    console.log("hll");

    let items = document.querySelectorAll(
      ".wx__image_files-container .wx__image-container"
    );
    items.forEach(function (item) {
      item.addEventListener("dragstart", handleDragStart, false);
      item.addEventListener("dragenter", handleDragEnter, false);
      item.addEventListener("dragover", handleDragOver, false);
      item.addEventListener("dragleave", handleDragLeave, false);
      item.addEventListener("drop", handleDrop, false);
      item.addEventListener("dragend", handleDragEnd, false);
    });
    // console.log("dd");
  });

  useEffect(() => {
    // TODO:: need to optimization and recheck before use
    // ISSUE:: cannot swap the image from same draggedFrom to same draggedTo
    const temp = imagesData;
    let from = temp[draggedFrom];
    let to = temp[draggedTo];
    let finalData: any[] = [];
    temp.forEach((item: any, index: number) => {
      let i = item;
      if (index === Number(draggedFrom)) i = to;
      if (index === Number(draggedTo)) i = from;
      finalData.push(i);
    });

    setImagesData(finalData);
  }, [draggedTo]);
  const onRemove = (index) => {
    const dd = imagesData.filter((item, i) => i !== index);
    setImagesData(dd);
  };

  return (
    <div>
      <div className="wx__card">
        <div>
          <p className="media_text _h6__semibold">Media</p>
          <div
            id="upload-container"
            className="file-area d-flex align-items-center justify-content-center"
          >
            <div className="file-dummy">
              <div className="success">
                Great, your files are selected. Keep on.
              </div>
              <div className="default d-flex align-items-center justify-content-center">
                <span className="material-icons-round img_icon">image</span>
                <div className="upload_info d-flex flex-column ">
                  <p>
                    Drag and Drop images here or{" "}
                    <label htmlFor="upload-files">
                      Browse on your computer
                    </label>
                  </p>
                  <span>Recomended size:1000px X 1000px</span>
                </div>
              </div>
            </div>
            <input
              type="file"
              name="images"
              id="upload-files"
              accept="image/*"
              multiple={true}
              onChange={(e: any) =>
                setImagesData([...imagesData, ...e.target.files])
              }
            />
          </div>
        </div>
      </div>

      <div id="image-container" className="wx__image_files-container">
        {imagesData?.map((addedFile: any, index: number) => {
          return (
            <div
              draggable={true}
              id={`div${index}`}
              data-index={index}
              className="wx__image-container"
              key={index}
            >
              <img
                className="wx__image"
                src={URL.createObjectURL(addedFile)}
                alt={addedFile.name}
              />
              <div className="image_hover">
                <div className="image_hover_icon">
                  <span className="material-symbols-outlined image-drag">
                    drag_indicator
                  </span>
                  <span
                    className="material-symbols-rounded image_remove"
                    role="button"
                    onClick={() => onRemove(index)}
                  >
                    close
                  </span>
                </div>
                <div>
                  <span className="material-icons-round img_icon">image</span>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default DragNDropNew;
