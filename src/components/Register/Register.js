import React from "react";
import "./Register.scss";

const Register = () => {
  console.log("hello world");

  return (
    <div className="login-main mt-4">
      <div className="text-center">
        <h1 className="login__getstarted__text">Get Started</h1>
        <p className="already__have__account__text">
          Already have an account?<span className="login__text"> Log in</span>
        </p>
      </div>
      <div>
        <form action="" className="d-flex flex-column">
          <div className="d-flex flex-column mt-md-3 mt-sm-2">
            <label htmlFor="" className="login__label__text mb-2">
              Email address or phone number
            </label>
            <input type="text" className="login__input" />
          </div>
          <div className="d-flex flex-column mt-md-3 mt-sm-2">
            <div className="d-flex flex-row justify-content-between">
              <label htmlFor="" className="login__label__text mb-2">
                Password
              </label>
              <p className="forgot__password__text">Forgot Password?</p>
            </div>
            <input type="text" className="login__input" />
          </div>
          <div className="d-flex flex-column mt-md-3 mt-sm-2">
            <label htmlFor="" className="login__label__text mb-2">
              Shop name
            </label>
            <div className="position-relative d-flex flex-row">
              <input type="text" className="login__input w-100" />
              <div className="position-absolute help__icon__parent">
                {/* <img className="help__icon" src={helpIcon} alt="" /> */}
                <span className="material-icons-round help__icon">help</span>
              </div>
            </div>
          </div>
          <button className="create__webx__id__btn py-2">Create WebX ID</button>
          <div>
            <p className="terms__n__services__text">
              Signing up for a Webx Global means you agree to the{" "}
              <span className="text-decoration-underline">Privacy policy</span>{" "}
              and{" "}
              <span className="text-decoration-underline">
                Terms of Service
              </span>
              .
            </p>
          </div>
        </form>
      </div>
      <div></div>
    </div>
  );
};

export default Register;
