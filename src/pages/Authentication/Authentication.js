import React from "react";
import webXLogo from "../../assets/svg/webx-logo.svg";
import Font from "../../sass/Font";
import "./Authentication.scss";

const Authentication = () => {
  return (
    <>
      <div className="authentication__main">
        <div className="">
          <img src={webXLogo} className='d-block mx-auto pt-3' alt="" />
        </div>
        {/* <Login/>
        <Register /> */}
        <Font/>
        <div className="bottom__terms">
            <ul className="d-flex justify-content-center p-0">
                <li>
                    <p>Help</p>
                </li>
                <li>
                    <p className="mx-3">Privacy</p>
                </li>
                <li>
                    <p className="">Terms</p>
                </li>
            </ul>
        </div>
      </div>
    </>
  );
};

export default Authentication;
