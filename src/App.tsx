import "bootstrap/dist/css/bootstrap.min.css";
import DragNDropNew from "components/DragNDrop/DragNDrop";
import { useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./App.scss";
import WxText from "./components/Typography/WxText";
import BootPopover from "./theme/BootPopover";
import BootTooltip from "./theme/BootTooltip";
import Buttons from "./theme/Buttons";
import Font from "./theme/Font";
import Inputs from "./theme/Forms/Inputs";
import Switch from "./theme/Forms/Switch";
import SideNav from "./theme/SideNav";
import Table from "./theme/Table";
import Tabs from "./theme/Tabs";
import Tags from "./theme/Tags";
import Toast from "./theme/Toast";
import Tooltip from "./theme/Tooltip";
import WXToast, { wxToastSlice } from "./theme/WXToast";

function App() {
  const [imagesData, setImagesData] = useState([]);
  const [text, setText] = useState([]);

  console.log(imagesData);
  const clickToCopy = (e: any) => {
    navigator.clipboard.writeText(e.target.className);
    toast("class name copied! 👍✨", {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "dark",
    });
  };

  const handleClick = () => {
    toast(
      "Please check you internet connection and contact admin manager to further escalate the issue. 👍✨",
      {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      }
    );

    wxToastSlice(
      "Please check you internet connection and contact admin manager to further escalate the issue.",
      {
        position: "top-left",
      }
    );
  };

  const wXClick = () => {
    toast("class name copied! 👍✨", {
      position: "top-center",
      autoClose: 500000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });

    wxToastSlice("hello World", {
      // position: "top-center",
      type: "success",
    });
  };

  // bootstrap start

  // end

  return (
    <>
      {/* <Authentication/> */}
      {/* <Register/> */}
      {/* <Header></Header> */}
      <ToastContainer />

      <div className="">
        <h1 className="text-center bg-primary border border-3 border-dark p-2 text-white mt-2">
          Font Styles
        </h1>
        {/* Typography Components */}

        <WxText
          size="btn_big"
          weight="regular"
          // decoration="strikethrough"
          // style="italic"
        >
          Button/Big
        </WxText>

        {/* ///////// */}
        <Font clickToCopy={clickToCopy} />
      </div>
      <div>
        <h1 className="text-center bg-primary border border-3 border-dark p-2 text-white mt-2">
          Buttons Styles
        </h1>
        <Buttons clickToCopy={clickToCopy} />
      </div>
      <div>
        <h1 className="text-center bg-primary border border-3 border-dark p-2 text-white mt-2">
          Toast Alerts Styles
        </h1>
        <Toast />
      </div>
      <div>
        <h1 className="text-center bg-primary border border-3 border-dark p-2 text-white mt-2">
          Tooltip Styles
        </h1>
        <Tooltip />
      </div>
      <div>
        <h1 className="text-center bg-primary border border-3 border-dark p-2 text-white mt-2">
          Navigation Styles
        </h1>
        <div className="row py-5">
          <div className="col-5 align-self-center" style={{ width: "240px" }}>
            <div
              className="rounded"
              style={{ width: "240px", background: "skyblue" }}
            >
              <SideNav>
                <div className="wx__single__side__nav">
                  <span className="material-icons-round">home</span>
                  <div className="wx__side__navigation__text">
                    <span>Level 01</span>
                  </div>
                </div>
                <div className="wx__single__side__nav">
                  <span className="material-icons-round">receipt</span>
                  <div className="wx__side__navigation__text">
                    <span>Level 02</span>
                  </div>
                </div>
                <div className="wx__single__side__nav">
                  <span className="material-icons-round">category</span>
                  <div className="wx__side__navigation__text">
                    <span>Level 03</span>
                  </div>
                </div>
                <div className="wx__single__side__nav">
                  <span className="material-icons-round">person</span>
                  <div className="wx__side__navigation__text">
                    <span>Level 04</span>
                  </div>
                </div>
                <div className="wx__single__side__nav">
                  <span className="material-icons-round">local_offer</span>
                  <div className="wx__side__navigation__text">
                    <span>Level 05</span>
                  </div>
                </div>
              </SideNav>
              {/* <Sidebar /> */}
            </div>
          </div>
          <div className="col-7 ms-auto">
            <iframe
              title="iframe for sidenav"
              width="100%"
              height="300"
              src="//jsfiddle.net/TituX/g815bprd/1/embedded/html/"
              allowFullScreen
              frameBorder={0}
            ></iframe>
          </div>
        </div>
      </div>
      <div>
        <h1 className="text-center bg-primary border border-3 border-dark p-2 text-white mt-2">
          Forms Styles
        </h1>
        <Inputs />
        <Switch />
      </div>
      <div>
        <h1 className="text-center bg-primary border border-3 border-dark p-2 text-white mt-2">
          Bootstrap Tooltip Styles
        </h1>
        <BootTooltip />
        <div>/////////////////////////////////////////////////////</div>
        <BootPopover />
      </div>
      <div>
        <h1 className="text-center bg-primary border border-3 border-dark p-2 text-white mt-2">
          Tags Styles
        </h1>
        <Tags />
      </div>
      <div>
        <h1 className="text-center bg-primary border border-3 border-dark p-2 text-white mt-2">
          Tabs Styles
        </h1>
        <Tabs />
      </div>
      <div style={{ height: "100vh" }}>
        <h1 className="text-center bg-primary border border-3 border-dark p-2 text-white mt-2">
          Tables Styles
        </h1>
        <Table />
      </div>
      {/* this is initial wrapper for toast */}
      <WXToast />

      {/* <WXEditor /> */}
      {/* <SCRPTVERSE /> */}
      {/* <CKeditor /> */}
      {/* <W3table /> */}
      <br />
      <br />
      <DragNDropNew imagesData={imagesData} setImagesData={setImagesData} />
      <br />
      <br />
      <br />
    </>
  );
}
export default App;
